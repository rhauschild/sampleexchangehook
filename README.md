# Sample Exchange Hook


## How does it work?

3D printed hook to exchange samples without the need for draining and un-installing the chamber. Especially for viscous (e.g. CUBIC), hard to clean or bubble prone solutions.

#


<img src="Photo2.jpg" width=45% />
<img src="Photo1.jpg" width=45% />
<img src="render.jpg" width=45% />

## Status

works. 

## Acknowledgment

Mohammad Goudarzi came up with the idea, the original desgin, and tested various prototypes.

This project has been made possible in part by CZI grant DAF2020-225401 and grant DOI https://doi.org/10.37921/120055ratwvi from the Chan Zuckerberg Initiative DAF, an advised fund of Silicon Valley Community Foundation (funder DOI 10.13039/100014989).